from flask import request, jsonify
from flask_login import login_user, login_required, current_user, logout_user
from flask_wtf.csrf import generate_csrf

from environment_data.storage import get_latest_environment_data
from heating_api import app, bcrypt, csrf, scheduler, boiler
from heating_api.models import User
from heating_control.thermostat import get_heating_mode, thermostat_check, stop_auto_mode, HeatingModes, \
    THERMOSTAT_JOB_ID


@app.after_request
def inject_csrf_token(response):
    response.headers.set('X-CSRF-Token', generate_csrf())
    return response


# @app.route('/signin', methods=['POST'])
# def signin():
#     data = request.get_json()
#     me = User(username=data['username'], email=data['email'], password=data['password'])
#     db.session.add(me)
#     db.session.commit()
#     return 'Hello, World!'


@app.route('/login', methods=['POST'])
@csrf.exempt
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = User.query.filter_by(username=username).first()
    if not user or not bcrypt.check_password_hash(user.password, password):
        return jsonify({"msg": "Wrong username or password"}), 400
    else:
        login_user(user, remember=True)
        return jsonify({
            "msg": "Successfully Logged In",
            "user": {
                "username": user.username,
                "email": user.email,
            },
        }), 200


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    return jsonify({"msg": "Successfully Logged Out"}), 200


@app.route('/protected', methods=['GET'])
@login_required
def protected():
    return jsonify(user=current_user), 200


@app.route('/environment', methods=['GET'])
@login_required
def get_environmental_data():
    temperature_data = get_latest_environment_data()
    temperature_data['temperature'] = round(temperature_data['temperature'], 2)
    temperature_data['humidity'] = round(temperature_data['humidity'], 2)
    return jsonify(temperature_data), 200


@app.route('/heating', methods=['GET', 'PUT'])
@login_required
def heating_status():
    if request.method == 'PUT':
        if get_heating_mode() == HeatingModes.auto:
            stop_auto_mode()
        data = request.get_json()
        if data['heating_on']:
            boiler.turn_on()
        else:
            boiler.turn_off()
    is_heating_on = boiler.is_on
    return jsonify(heating_on=is_heating_on, mode=get_heating_mode().value), 200


@app.route('/thermostat', methods=['GET', 'PUT', 'DELETE'])
@login_required
def thermostat():
    current_thermostat_job = scheduler.get_job(THERMOSTAT_JOB_ID)
    if request.method == 'PUT':
        data = request.get_json()
        if current_thermostat_job is None:
            scheduler.add_job(thermostat_check, 'interval', seconds=10, args=[data['temp']], id=THERMOSTAT_JOB_ID)
        else:
            current_thermostat_job.modify(args=[data['temp']])
    elif request.method == 'DELETE':
        if current_thermostat_job is not None:
            stop_auto_mode()
            if boiler.is_on:
                boiler.turn_off()
    updated_thermostat_job = scheduler.get_job(THERMOSTAT_JOB_ID)
    is_thermostat_on = updated_thermostat_job is not None
    thermostat_temp = None if not is_thermostat_on else updated_thermostat_job.args[0]
    return jsonify(thermostat_on=is_thermostat_on, thermostat_temp=thermostat_temp), 200
