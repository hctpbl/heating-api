from heating_api.settings.default_settings import *

ENV = 'development'
DEBUG = True

INFLUX_DB['PORT'] = 3333
MQTT_BROKER_PORT = 3335
