import os

from heating_api import secrets

HEATING_API_ROOT_PATH = os.path.abspath(os.getcwd())

SECRET_KEY = secrets.secret_key

DEBUG = False
TESTING = False

DATABASE_FILE_NAME = 'heating_api.db'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(HEATING_API_ROOT_PATH, DATABASE_FILE_NAME)
SQLALCHEMY_TRACK_MODIFICATIONS = False

INFLUX_DB = {
    'HOST': 'localhost',
    'PORT': 8086,
}

MQTT_BROKER_URL = '127.0.0.1'
MQTT_BROKER_PORT = 1883
MQTT_USERNAME = ''
MQTT_PASSWORD = ''
MQTT_KEEPALIVE = 5
MQTT_TLS_ENABLED = False


HYSTERESIS = 1
