from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, abort, jsonify, make_response
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_login import LoginManager, current_user
from flask_migrate import Migrate
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from influxdb import InfluxDBClient

from heating_control.boiler import Boiler

app = Flask(__name__)

app.config.from_object('heating_api.settings.default_settings')
app.config.from_envvar('HEATING_API_SETTINGS')

csrf = CSRFProtect(app)
CORS(app, supports_credentials=True, expose_headers='X-CSRF-Token')
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)

# initialize scheduler
scheduler = BackgroundScheduler({'apscheduler.timezone': 'Europe/Madrid'})
scheduler.start()

# initialize influxdb client
influx_config = app.config['INFLUX_DB']
influx_client = InfluxDBClient(host=influx_config['HOST'], port=influx_config['PORT'], database='home')

mqtt = Mqtt(app)
socketio = SocketIO(app, cors_allowed_origins='*')
boiler = Boiler(mqtt, socketio)

import heating_api.views
import heating_api.models


@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(int(user_id))


@login_manager.unauthorized_handler
def unauthorized_response():
    abort(make_response(jsonify("Not authorized"), 401, {'WWW-Authenticate':'Basic realm="Login Required"'}))


# Update clients state when they get connected
@socketio.on('connect')
def on_client_connect(*args, **kwargs):
    if current_user.is_authenticated:
        boiler.request_update()
    else:
        return False  # not allowed here


if __name__ == '__main__':
    socketio.run(app)
