from heating_api import influx_client


def get_latest_environment_data():
    result = influx_client.query('select temperature, humidity, time from environment order by time desc limit 1;')
    return list(result.get_points())[0]
