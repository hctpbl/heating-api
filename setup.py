from setuptools import setup

setup(
    name='heating_api',
    packages=['heating_api'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-sqlalchemy',
        'flask-migrate'
    ],
)