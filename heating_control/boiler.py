HEATING_MQTT_RESOURCE_NAME = 'heating'


class Boiler:

    def __init__(self, mqtt_app, socketio_app):
        self.__is_on = False
        self.__mqtt_app = mqtt_app
        self.__socketio_app = socketio_app

        # Functions to react to mqtt events related to heating
        @self.__mqtt_app.on_connect()
        def handle_connect(client, userdata, flags, rc):
            self.__mqtt_app.subscribe('stat/{}/#'.format(HEATING_MQTT_RESOURCE_NAME), qos=1)

        @self.__mqtt_app.on_message()
        def _handle_mqtt_heating_stat_message(client, userdata, message):
            if message.topic == 'stat/{}/POWER'.format(HEATING_MQTT_RESOURCE_NAME):
                is_heating_on = message.payload.decode() == 'ON'
                self.is_on = is_heating_on

    @property
    def is_on(self):
        return self.__is_on

    @is_on.setter
    def is_on(self, is_heating_on):
        self.__is_on = is_heating_on
        self._send_update_to_clients()

    def request_update(self):
        self.__mqtt_app.publish('cmnd/{}/POWER'.format(HEATING_MQTT_RESOURCE_NAME))

    def _send_update_to_clients(self):
        self.__socketio_app.emit('updateHeatingStatus', data=self.is_on)

    def turn_on(self):
        self.__mqtt_app.publish('cmnd/{}/POWER'.format(HEATING_MQTT_RESOURCE_NAME), 'ON')

    def turn_off(self):
        self.__mqtt_app.publish('cmnd/{}/POWER'.format(HEATING_MQTT_RESOURCE_NAME), 'OFF')
