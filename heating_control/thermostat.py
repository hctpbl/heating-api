from enum import Enum

from environment_data.storage import get_latest_environment_data
from heating_api import app, scheduler, boiler

THERMOSTAT_JOB_ID = 'thermostat'


def thermostat_check(target_temperature):
    temperature_data = get_latest_environment_data()
    current_temperature = temperature_data['temperature']
    is_heating_on = boiler.is_on
    # Turn heating on taking Hysteresis into account
    if current_temperature < target_temperature - app.config['HYSTERESIS'] and not is_heating_on:
        boiler.turn_on()
    elif current_temperature >= target_temperature and is_heating_on:
        boiler.turn_off()


class HeatingModes(Enum):
    auto = 'auto'
    manual = 'manual'


def get_heating_mode():
    return HeatingModes.auto if scheduler.get_job(THERMOSTAT_JOB_ID) is not None else HeatingModes.manual


def stop_auto_mode():
    current_thermostat_job = scheduler.get_job(THERMOSTAT_JOB_ID)
    if current_thermostat_job is not None:
        current_thermostat_job.remove()
